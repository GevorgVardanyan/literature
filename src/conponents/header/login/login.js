import React from 'react';
import css from "./login.module.scss"
import { MdAssignmentInd } from "react-icons/md";

const Login = () => {
    return (
        <div className={css.login} >
            <a className={css.loginIcon} >
                <MdAssignmentInd/>
            </a>
        </div>
    );
};

export default Login;