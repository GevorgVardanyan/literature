import React from 'react';
import css from "./menu.module.scss"
import  {authRoutes,routes} from "../../pages/routes";
import {Link} from "react-router-dom";
import  {useLocation} from "react-router-dom";
import Login from "../login/login";
import Button from "../../ui/button/button";
import Logout from "../../pages/logout/logout";
import sidebar from "../../modals/sidebar";

const Menu = () => {
    const isAuth =  localStorage.getItem("token");

    const {pathname}= useLocation()


    return (
        <div className={css.menu} >

            <ul>
                {isAuth ?
                    <>{
                          authRoutes.map(({name, path, sidebar,}) => {
                              return !sidebar && (
                                  <li key={path} exact="true" className={pathname === path ? css.active : undefined}>
                                  {name === "Logout" ? <Logout />:
                                      <Link to={path}>
                                          {name}
                                      </Link>
                                  }

                              </li>)

                          })
                      }
                      {/*<Button value={"logout"} onClick={handleClick} />*/}
                    </>

                    :routes.map(({name,path})=>{
                        return (<li key={path} exact="true" className={pathname===path?css.active:undefined}>
                            <Link to ={path} >
                                {name==='Sign' ?
                                    <Login/> :
                                    name}
                            </Link>
                        </li>)
                    })

                }

            </ul>


        </div>
    );
};

export default Menu;