import React from 'react';
import css from "./logo.module.scss"
import logo from"../../images/logo.png"
const Logo = () => {
    return (
        <div className={css.logo}>
            <img className={css.icon} src={logo} alt="logo"/>
        </div>
    );
};

export default Logo;