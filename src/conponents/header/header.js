import React from 'react';
import Logo from "./logo/logo";
import Menu from "./menu/menu";
import css from "./header.module.scss";
import Login from "./login/login"
const Header = () => {
    return (
        <div className={css.header} >
            <Logo/>
            <Menu/>


        </div>
    );
};

export default Header;