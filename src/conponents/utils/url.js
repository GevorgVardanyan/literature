export const HOME_PAGE ="/"
export const BOOKS_PAGE ="/books"
export const ABOUT_PAGE ="/about"
export const HELP_PAGE ="/help"
export const SIGN_IN ="/login"
export const LOGOUT="/logout"
export const POSTS_PAGE="/posts"
export const COMMENTS_PAGES="/comments"



export const CHILDREN_BOOK_PAGE = "/children-book"
export const FICTION_PAGE = "/fiction"
export const NONFICTION_PAGE = "/non-fiction"
