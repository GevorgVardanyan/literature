import {emailRegex, firstnameRegex, lastnameRegex, passwordRegex, phoneNumberRegex, } from "./regex";


export const  emailValidation = {
    required: "Required",
    pattern: {value:emailRegex,message: "Wrong email"}

}
export const passwordValidation ={
    required: "Required ",
    pattern:{value:passwordRegex , message: "Wrong password"},
    maxLength : {
        value: 15,message: 'error message'}
}
export const firstnameValidation ={
    required:"Required",
    pattern:{value:firstnameRegex,message:"Wrong FirstName"}
}
export const lastnameValidation = {
    required:"Required",
    pattern:{value:lastnameRegex,message:"Wrong LastName"}
}
export const phoneNumberValidation ={
    required:"Required",
    pattern:{value:phoneNumberRegex,message:"Wrong phoneNumber"}
}
