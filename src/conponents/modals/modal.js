import React from 'react';
import css from './modal.module.scss'
import cn from "classnames"

const Modal = ({children,close}) => {

    return (

        <div className={css.mask} onClick={close} >
            <div className={cn(css.modal, css.sidebar)} onClick={e => e.stopPropagation()}>
                {children}
            </div>
        </div>
    );
};

export default Modal;