import React from 'react';
import Modal from "./modal";
import css from "./modal.module.scss"
import {authRoutes} from "../pages/routes";
import {Link, useLocation} from "react-router-dom";

const Sidebar = ({close}) => {

    const isAuth = true
    const {pathname}= useLocation()


    return (

        <Modal close={close} >

            <ul className={css.menu}>
                {
                    isAuth
                    && authRoutes.map(({path, name, sidebar}) => {
                        return sidebar && <li key={path}>
                            <Link
                                className={pathname === path ? css.active : undefined}
                                to={path}
                                exact='true'
                                onClick={close}
                            >
                                {name}
                            </Link>
                        </li>
                    })
                }
            </ul>

        </Modal>
    );
};

export default Sidebar;