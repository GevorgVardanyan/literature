import React from 'react';
import {shopInfo} from "../../ui/shopinfo/shopinfo"
import Book from "../../ui/shopitems/book";
import css from './books.module.scss'
import img1 from "../../images/XachaturAbovyan.jpg"
import img2 from "../../images/raffi.jpg"
import img3 from "../../images/hovhannestumanyan.jpg"
import img4 from "../../images/hovhanesshiraz.jpg"
import img5 from "../../images/hamoSahyan.jpg"
import img6 from "../../images/danielvarujan.jpg"
import img7 from "../../images/charenc.jpg"
import img8 from "../../images/sevak.jpg"
import img9 from "../../images/nonFiction book1.jpg"
import img10 from "../../images/nonFiction book2.jpg"
import img11 from "../../images/nonFiction book3.jpg"
import img12 from "../../images/nonFiction book4.jpg"
import img13 from "../../images/nonFiction book5.jpg"
import img14 from "../../images/childrenbook1.jpg"
import img15 from "../../images/childrenbook2.jpg"
import img16 from "../../images/childrenbook3.jpg"
import img17 from "../../images/childrenbook4.jpg"
import {useLocation, useNavigate} from "react-router-dom";
import {useQuery} from "../../../hooks/useQuery";

const categories = [
    {
        id:1 ,name:"fiction"
    },
    {
        id:2 ,name:"nonFiction"
    },
    {
        id:3,name:"children"
    }
]
const book =[

    {
        id:Math.random(), categoryId:1 , img:img1,books:["Վերք Հայաստանի","Պարապ վախտի խաղալիքը"], name:"Խաչատուր Աբովյան"
    },
    {
        id:Math.random(), categoryId:1 , img:img2,books:[""], name:""
    },
    {
        id:Math.random(), categoryId:1 , img:img3,books:[""], name:""
    },
    {
        id:Math.random(), categoryId:1 , img:img4,books:[""], name:""
    },
    {
        id:Math.random(), categoryId:1 , img:img5,books:[""], name:""
    },
    {
        id:Math.random(), categoryId:1 , img:img6,books:[""], name:""
    },
    {
        id:Math.random(), categoryId:1 , img:img7,books:[""], name:""
    },
    {
        id:Math.random(), categoryId:1 , img:img8,books:[""], name:""
    },
    {
        id:Math.random(), categoryId:2 , img:img9,books:[""], name:""
    },
    {
        id:Math.random(), categoryId:2 , img:img10,books:[""], name:""
    },
    {
        id:Math.random(), categoryId:2 , img:img11,books:[""], name:""
    },
    {
        id:Math.random(), categoryId:2 , img:img12,books:[""], name:""
    },
    {
        id:Math.random(), categoryId:2 , img:img13,books:[""], name:""
    },
    {
        id:Math.random(), categoryId:3 , img:img14,books:[""], name:""
    },
    {
        id:Math.random(), categoryId:3 , img:img15,books:[""], name:""
    },
    {
        id:Math.random(), categoryId:3 , img:img16,books:[""], name:""
    },
    {
        id:Math.random(), categoryId:3 , img:img17,books:[""], name:""
    }
]

const Books = () => {
    const categoryQuery = +useQuery().get('category')
    const {pathname}=useLocation()
    const navigate=useNavigate()

    const changeCategory = (categoryId)=>{
        navigate(`${pathname}?category=${categoryId}`)
    }

    return (
      <div className={css.booksContainer}>
          <ul className={css.categories} >
              <li
                onClick={() => {
                    navigate(`${pathname}`)
                }}
                className={!categoryQuery?css.active:undefined}
              >All</li>
              {
                  categories.map(({id,name})=>{
                      return <li key={id}
                                 className={id === categoryQuery ? css.active : undefined}
                                 onClick={()=>changeCategory(id)}
                      >
                          {name}
                      </li>
                  })
              }
          </ul>
          <ul className={css.imgContainer} >
              {

                      book.map(({id, name, img, categoryId}) => {
                          if(!categoryQuery){
                              return <li key={id}>
                                  <img src={img} alt="img"/>
                                  <h4>{name}</h4>
                              </li>
                          }
                          return categoryId === categoryQuery && <li key={id}>
                              <img src={img} alt="img"/>
                              <h4>{name}</h4>
                          </li>
                      })

              }
          </ul>
          <button className={css.btn}><i className="fa fa-download"></i> Download</button>
      </div>
    );
};

export default Books;