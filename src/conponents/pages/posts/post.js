import React, {useEffect, useState} from 'react';
import {get} from "react-hook-form"
import {usePost} from "../../../provider/postProvider";
import css from"./post.module.scss"
import {useQuery} from "../../../hooks/useQuery";
import {makeArray} from "../../utils/helper";
import {useLocation, useNavigate} from "react-router-dom";

const Post = () => {
    const pageQuery=+useQuery().get(`page`) || 1
    const limitQuery=+useQuery().get(`limit`)|| 10
    const navigate = useNavigate()
    const {pathname}= useLocation()

    const {posts, getPosts} = usePost()

    const[page,setPage]=useState(pageQuery)
    const [limit,setLimit]=useState(limitQuery)
    const [pageCount,setPageCount]=useState(10)
    const [pages,setPages]=useState(makeArray(pageCount))
    const[search,setSearch]=useState("")
    const[searchedPost,setSearchedPost]=useState([])


    useEffect(() => {
        getPosts(page, limit,)
    }, [page,limit])

    useEffect(() => {
        setPage(pageQuery)
        setPageCount(Math.ceil(100 / limit))
    }, [limit])

    useEffect(() => {

        setPages(makeArray(pageCount))

    }, [pageCount, limit])

    useEffect(()=>{
        if(posts.length){
            setSearchedPost(posts)
        }
    },[posts])

    return (
        <div className={css.postPage} >
            <input
                type="text"
                value={search}
                className={css.input}
                onChange={e =>{
                    // const searched = searchedPost.filter(p => p.title.includes(e.target.value))
                    const searched = posts.filter(p => p.title.toLowerCase().includes(e.target.value.toLowerCase()))
                    setSearchedPost(searched)
                    console.log(searched,"error")
                      setSearch(e.target.value)
                }

                }

            />
            <ul>
                <select
                    className={css.select}
                    value={limitQuery}
                    onChange={e => {
                        setLimit(+e.target.value)
                        navigate(`${pathname}?page=${page}&limit=${e.target.value}`)
                    }}
                >
                    <option value="5" >5</option>
                    <option value="10" >10</option>
                    <option value="40" >40</option>
                    <option value="80" >80</option>
                </select>
            </ul>
            <ul className={css.post} >
                {
                    searchedPost.map(({id, title}) => {
                        return <li key={id}>
                            {title}
                        </li>
                    })
                }

            </ul>
            <ul className={css.pages} >
                {
                    pages.map(p=>{
                        return <li
                            key={p}
                            className={pageQuery === p ? css.active : undefined}
                            onClick={() => {
                                setPage(p)
                                navigate(`${pathname}?page=${p}&limit=${limit}`)
                            }}
                        >
                            {p}

                        </li>
                    })
                }
            </ul>
        </div>
    );
};

export default Post;