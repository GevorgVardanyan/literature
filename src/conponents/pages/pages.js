import React from 'react';
import {Route, Routes, Navigate} from "react-router-dom";
import {authRoutes,routes} from "./routes";
import Button from "../ui/button/button";
import ChildrenBook from "./books/childrenBooks/childrenBook";
import Fiction from "./books/fiction/fiction";
import NonFiction from "./books/nonFiction/nonFiction";

const Pages = () => {

    const isAuth = localStorage.getItem("token");

    return (
      <Routes>

        <Route
          path={'/children-books'}
          element={<ChildrenBook/>}
          exact="true"
        />
        <Route
          path={'/fiction'}
          element={<Fiction/>}
          exact="true"
        />
        <Route
          path={'/non-fiction'}
          element={<NonFiction/>}
          exact="true"
        />
            { isAuth ?
           authRoutes.map(({path,element })=>{

                return <Route
                    key={path}
                    path={path}
                    element={element}
                    exact="true"
                />
            })


            :routes.map(({path,element })=>{
                return <Route
                    key={path}
                    path={path}
                    element={element}
                    exact="true"
                />
            })

            }


        </Routes>


    );
};

export default Pages;