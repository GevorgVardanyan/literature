import React from 'react';
import {useNavigate} from "react-router-dom";
import css from "./logout.module.scss"

const Logout = ({}) => {
    const navigate= useNavigate()
    const handleClick = () => {
        localStorage.removeItem('token');
        navigate ("/login?form=login")
        window.location.reload()
    }
    return (
        <button onClick={handleClick} className={css.logbtn} >
            Logout
        </button>
    );
};

export default Logout;