import React from 'react';
import {
    HOME_PAGE,
    BOOKS_PAGE,
    ABOUT_PAGE,
    HELP_PAGE,
    SIGN_IN,
    LOGOUT,
    POSTS_PAGE,
    COMMENTS_PAGES
} from "../utils/url";
import Home from "./home/home";
import Books from "./books/books";
import About from "./about/about";
import Help from "./help/help";

import Form from "../ui/form/form";
import Button from "../ui/button/button";
import Post from "./posts/post";
import Comments from "./coments/comments";

export  const routes=[
    {
        name:"Help",
        path:HELP_PAGE,
        element:<Help/>
    },
    {
        name:"Sign",
        path:SIGN_IN,
        element:<Form/>
    }


]
 export const authRoutes = [
     {
         name:"Home",
         path:HOME_PAGE,
         element:<Home/>
     },
     {
         name:"Books",
         path:BOOKS_PAGE,
         element:<Books/>
     },
     {
         name:"About",
         path:ABOUT_PAGE,
         element:<About/>
     },
     {
         name:"Logout",
         path:LOGOUT,
         element: <Button/>
     },
     {
         name:"Posts",
         path: POSTS_PAGE,
         element: <Post/>,
         sidebar: true
     },
     {
         name:"Comments",
         path:COMMENTS_PAGES,
         element: <Comments/>,
         sidebar: true
     }

]
