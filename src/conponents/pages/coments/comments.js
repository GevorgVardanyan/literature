import React, {useEffect, useState} from 'react';
import {useComments} from "../../../provider/commentProvider";
import css from "./commets.module.scss"
import {makeArray} from "../../utils/helper";
import {useLocation, useNavigate} from "react-router-dom";
import {useQuery} from "../../../hooks/useQuery";
const Comments = () => {
    const{comments,getComments}=useComments()
    const pageQuery = useQuery().get("page") || 1
    const limitQuery = useQuery().get("limit")

    const navigate = useNavigate()
    const{pathname} = useLocation()


    const [value,setValue]=useState(10)
    const[page,setPage]=useState(pageQuery)  /*yntacik ej*/
    const[limit,setLimit]=useState(limitQuery)  /* qani hat tpe mi ejm*/
    const[pageCount,setPageCount]=useState(10)   /*ejeri qanak*/
    const[pages,setPages]=useState(makeArray(pageCount))   /*ejer*/
    const[search,setSearch]=useState("")
    const[searchedComment,setSearchedComment]=useState([])

    const enter=()=>{
        setLimit(value)
    }

    const toSearch= (e)=>{
        const searched =comments.filter(p => p.name.toLowerCase().includes(search.toLowerCase()))
        setSearchedComment(searched)
        console.log("error")
    }

    useEffect(()=>{

        if(comments.length){
            setSearchedComment(comments)
        }

    },[comments])


    useEffect(()=>{
        getComments(page,limit)
    },[page,limit])


    useEffect(() => {
        setPage(pageQuery)
        setPageCount(limit ? Math.ceil(100 / limit) : 1)
    }, [limit])


    useEffect(()=>{
        setPages(makeArray(pageCount))
    },[pageCount])


    return (
        <div className={css.container} >
            <ul className={css.button} >

                <input
                    type={"text"}
                    value={value}
                    placeholder="enter number"
                    onChange={e=>
                        setValue(+e.target.value)
                    }
                >


                </input>
                <button
                    onClick={enter}
                >
                    Enter
                </button>

                <input
                    type="text"
                    value={search}
                    onChange={e=>{
                        setSearch(e.target.value)
                    }}

                />
                <button
                    onClick={toSearch}
                >search</button>

            </ul>


            <ul className={css.comment} >
                {
                    searchedComment.map(({id,name})=>{
                        return <li key={id} >
                            {
                                name
                            }
                        </li>
                    })
                }
            </ul>
            <ul className={css.pages} >
                {
                    pages.map(p=>{
                        return <li
                            key={p}
                            onClick={()=> {
                                setPage(p)
                                navigate(`${pathname}?page=${p}&limit=${limit}`)
                            }}
                        >
                            {p}
                        </li>
                    })
                }
            </ul>
        </div>
    );
};

export default Comments;