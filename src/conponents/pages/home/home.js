import React from 'react';
import {shopInfo} from "../../ui/shopinfo/shopinfo";
import Book from "../../ui/shopitems/book";
import css from "./home.module.scss"
const Home = () => {
    return (
        <div className={css.bookContainer}>
            {
                shopInfo.map((element)=>{
                    return(
                        <Book title={element.title} key={element.title} pathName={element.pathName} book={element.book} />
                    )
                })
            }
        </div>
    );
};

export default Home;