import React, {useState} from 'react';
import Button from "../../ui/button/button";
import css from "./about.module.scss"

const About = () => {
    const [about,setAbout] =useState([
        {id:Math.random(),name:"Learn HTML",},

    ])
    const [aboutName,setAboutName]=useState("")
    const [edite,setEdite]=useState({})
    const [textEdite,setTextEdite]=useState({})
    const [file,setFile] =useState('');
    const[image,setImage]=useState('')

    console.log()

    const addList =()=>{
        if(about){
            setAbout(about=>about.concat([{id:Math.random(),name:aboutName, fileName: file,imageName:image}]))
            // setFiles(files=>about.concat([{id: Math.random(), name: aboutName}]))
            console.log(about)
        }
    }
    const Edite=()=>{


    }
    const Delete=(id)=>{
        setAbout(about=>about.filter(about=>about.id!== id))
        console.log(about)

    }

    return (
        <div className={css.container} >
            <ul className={css.page} >
                {
                    about.map(({id, name, fileName,imageName})=>{
                        return <li
                            key={id}
                        >
                            {name}
                            <div>
                               <div className={css.imgContainer} >
                                   <img src={imageName || ''} alt ='book picture' />
                                   <embed
                                     src={fileName}
                                     width="80"
                                     height="80"/>

                               </div>

                                <button

                                >
                                    {edite.id ? "Save" : "Edite"}
                                </button>
                                <button
                                    onClick={()=> Delete(id)
                                    }
                                >Delete</button>
                            </div>
                        </li>

                    })
                }

            </ul>
            <ul className={css.fix} >



                <input
                    type="text"
                    value={aboutName}
                    onChange={(e)=>
                        setAboutName(e.target.value)
                    }
                />
                <input
                  type="file"
                  // value={file}
                  accept="pdf"
                  onChange={(e)=>

                    setFile(URL.createObjectURL(e.target.files[0]))}
                />
                <input
                type="file"
                // value={file}
                accept="image/png, image/jpeg,"
                onChange={(e)=>

                setImage(URL.createObjectURL(e.target.files[0]))}
                />

                <button
                    onClick={()=>{
                        addList(aboutName)
                        setAboutName("")
                    }}
                >Add</button>
            </ul>

        </div>
    );
};

export default About;