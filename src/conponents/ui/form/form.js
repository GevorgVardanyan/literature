import React from 'react';
import { Link, useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";

import Input from "../input/input";
import Button from "../button/button";

import Login from "../../header/login/login";
import { SIGN_IN } from "../../utils/url";
import {
  emailValidation, firstnameValidation,
  lastnameValidation,
  passwordValidation, phoneNumberValidation,

} from "../../utils/validation";
import { useQuery } from "../../../hooks/useQuery";

import css from"./form.module.scss";

const Form = () => {
  const navigate= useNavigate();
  const form =useQuery().get("form");
  const isFormLogin = form === "login";
  console.log('isFormLogin: ', isFormLogin);
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors, isValid }
  } = useForm({ mode: "all" });

  const onRegister = (data) => {
    let { username, email, password, firstname, confirmPassword } = data;
    localStorage.setItem(`user`, JSON.stringify({
      username,
      email,
      password,
      firstname,
      confirmPassword,
    }));
    navigate('/login?form=login');
    reset({});
  }

  const login = (data) => {
    let localEmail= JSON.parse(localStorage.getItem("user"))['email'];
    let localPassword= JSON.parse(localStorage.getItem("user"))['password'];
    localStorage.setItem("token", "fgdgfdgdfgd77777777gddgdgd");
    navigate("/");
    window.location.reload();
  }

  return (
    <form
      onSubmit={handleSubmit(  isFormLogin ? login: onRegister)}
      className={css.form}
    >
      {!isFormLogin &&
        <>
          <span>
            {errors.firstname?.message}
           </span>
          <Input
            name="firstname"
            placeholder={"FirstName"}
            type="text"
            register={register("firstname",firstnameValidation)}
          />
          <Input
            name="lastname"
            type="text"
            placeholder={"Lastname"}
            register={register("lastname", lastnameValidation)}
          />
          <span>
           {errors.lastname?.message}
          </span>
          <Input
            name="phoneNumber"
            type="text"
            placeholder={"PhoneNumber"}
            register={register("phoneNumber", phoneNumberValidation
            )}
          />
          <span>
            {errors.phoneNumber?.message}
          </span>
        </>
      }
      <Input
        name="email"
        placeholder={"Email"}
        type={"text"}
        register={register("email", emailValidation )}
      />
      <span>
        {errors.email?.message}
      </span>
      <Input
        name="password"
        placeholder={"Password"}
        type={"password"}
        register={register("password", passwordValidation)}
      />
      <span>
        {errors.password?.message}
      </span>
      {!isFormLogin &&
        <>
          <Input
            name="confirmPassword"
            placeholder={"Confirm Password"}
            type={"password"}
            register={register("confirmPassword", passwordValidation)}

          />
          <span>
            {errors.confirmPassword?.message}
          </span>
        </>}
      <Button disabled = {!isValid}  value={isFormLogin ? "Login" : "Register"} />
      {isFormLogin ?
        <span>
          For Register <Link to={SIGN_IN}> sign up </Link>
        </span>
        :
        <span>
          For Login <Link to={`${SIGN_IN}?form=login`} > sign in </Link>
        </span>
      }
    </form>
  );
};

export default Form;