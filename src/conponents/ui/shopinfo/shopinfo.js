import React from 'react';
import {CHILDREN_BOOK_PAGE} from "../../utils/url";
import ChildrenBook from "../../pages/books/childrenBooks/childrenBook";

export const shopInfo =[
    {
        title:"Fiction  Books",
        pathName: "/fiction",
        book:["book1","book2","book3","book4"],

    },
    {
        title:"Non-Fiction Books",
        pathName: "/non-fiction",
        book:["book5","book6","book7","book8"]
    },
    {
        title:"Children's Books",
        pathName: "/children-books",
        book:["book9","book10","book11","book12"],
    }
]
