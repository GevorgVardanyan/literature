import React from 'react';

const Button = ({value,...props}) => {

    return (
        <button {...props}  >

            {value}
        </button>
    );
};

export default Button;