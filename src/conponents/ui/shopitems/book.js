import React from 'react';
import css from "./book.module.scss"
import {Link} from "react-router-dom";
const Book = ({book, title, pathName}) => {
//
// console.log('path:: ', book, title, pathName)
    return (

            <div className={css.container} >
                <p> {title}</p>
                {
                  book.map((el)=>{
                        return (
                              <img src={`/images/${el}.jpg`} />
                        )
                    })
                }
              <Link to={pathName} className={css.link} >See more ...</Link>
            </div>

    );
};

export default Book;