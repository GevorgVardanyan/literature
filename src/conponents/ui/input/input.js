import React from 'react';

const Input = ({error,register,maxlength,...props}) => {
    return (
        <label>
           <div>
              <span>
                {error}
              </span>
           </div>
        <input
            {...maxlength}
            {...register}
            {...props}
    />
        </label>
    );
};

export default Input;