
import React from 'react';
import css from "./footer.module.scss"

const Footer = () => {
    const date = new Date().getFullYear()

    return (
        <footer className={css.footer}>
            &copy; Copyright {date}
        </footer>
    );
};

export default Footer;