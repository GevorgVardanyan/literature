import React, {createContext, useContext} from 'react';

const AboutContext = createContext({})
const AboutProvider = ({children}) => {


    return (
        <AboutContext.provider  >
            {children}
        </AboutContext.provider>
    );
};
const useAbout = ()=>{
    return useContext(AboutContext)
}

export default AboutProvider;