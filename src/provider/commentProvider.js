import React, {createContext, useContext, useState} from 'react';


const CommentsContext =createContext({})

const CommentsProvider = ({children}) => {

    const [comments,setComments]=useState([])

    const getComments = async (page,limit)=>{
        try {
            const response = await fetch(`https://jsonplaceholder.typicode.com/comments?_page=${page}&_limit=${limit}`)
            const data =await response.json()
            console.log(page)
            setComments(data)
        }catch (e){

        }


    }
    return (

        <CommentsContext.Provider value={{comments, getComments}} >
            {children}
        </CommentsContext.Provider>
    );
};

const useComments= ()=>{
    return useContext(CommentsContext)
}

export {CommentsProvider,useComments}