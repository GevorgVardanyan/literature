import {useState} from "react";

const {createContext, useContext} = require("react");

const PostContext = createContext({})

const PostProvider =({children})=>{
    const [posts, setPosts] = useState([])
    const getPosts = async (page,limit) => {
        try{
            const response = await fetch(`https://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=${limit}`)
            const data = await response.json()
            setPosts(data)

        }
        catch(e){


            console.log("error")
        }
    }

    return <PostContext.Provider value={{posts, getPosts}} >
        {children}
    </PostContext.Provider>
}
const usePost = ()=>{
    return useContext(PostContext)
}
export {PostProvider,usePost }