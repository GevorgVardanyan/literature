import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {BrowserRouter} from "react-router-dom";
import {PostProvider} from "./provider/postProvider";
import {CommentsProvider} from "./provider/commentProvider";
import AboutProvider from "./provider/aboutProvider";

ReactDOM.render(
    <BrowserRouter>
        <PostProvider>
            <CommentsProvider>
              <App/>
            </CommentsProvider>
        </PostProvider>

    </BrowserRouter>,

    document.getElementById('root')
);

