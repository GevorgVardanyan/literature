import React, {useState} from "react";
import Header from "./conponents/header/header";
import Footer from "./conponents/footer/footer";
import Pages from "./conponents/pages/pages";
import {CgChevronDoubleRightO} from "react-icons/cg";
import "./index.scss"
import Sidebar from "./conponents/modals/sidebar";

function App() {
    const [openSidebar, setOpenSidebar] = useState(false)
    return (
        <div className="App" >
     <div className="sidebarBtn" onClick={() => setOpenSidebar(true)}>
         <CgChevronDoubleRightO/>

     </div>
            {openSidebar && <Sidebar close={() => {

                setOpenSidebar(false)}
            } />}
             <Header/>
             <Pages />
             <Footer />

        </div>
    );
}

export default App;
